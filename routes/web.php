<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/ranking','HomeController@ranking')->name('ranking');



Route::get("/mall", "MallController@index")->name("mall.index");
Route::post("/mall/buy", "MallController@buy")->name("mall.buy");
Route::get("/mall/create", "MallController@create")->name("mall.create");
Route::post("/mall", "MallController@store")->name("mall.store");
Route::get("/mall/{mall}", "MallController@show")->name("mall.show");
Route::get("/mall/{mall}/edit", "MallController@edit")->name("mall.edit");
Route::put("/mall/{mall}", "MallController@update")->name("mall.update");
Route::delete("/mall/{mall}", "MallController@destroy")->name("mall.destroy");


Route::get("/storage", "StorageController@index")->name("storage.index");
Route::get("/storage/create", "StorageController@create")->name("storage.create");
Route::post("/storage", "StorageController@store")->name("storage.store");
Route::get("/storage/{storage}", "StorageController@show")->name("storage.show");
Route::get("/storage/{storage}/edit", "StorageController@edit")->name("storage.edit");
Route::put("/storage/{storage}", "StorageController@update")->name("storage.update");
Route::delete("/storage/{storage}", "StorageController@destroy")->name("storage.destroy");


Route::group(["namespace" => "Website"], function () {
    Route::get("/contact", "ContactController@index")->name("contact.index");
    Route::get("/contact/create", "ContactController@create")->name("contact.create");
    Route::post("/contact", "ContactController@store")->name("contact.store");
    Route::get("/contact/{contact}", "ContactController@show")->name("contact.show");
    Route::get("/contact/{contact}/edit", "ContactController@edit")->name("contact.edit");
    Route::put("/contact/{contact}", "ContactController@update")->name("contact.update");
    Route::delete("/contact/{contact}", "ContactController@destroy")->name("contact.destroy");

    Route::get("/downloads", "DownloadController@index")->name("downloads.index");
    Route::get("/downloads/create", "DownloadController@create")->name("downloads.create");
    Route::post("/downloads", "DownloadController@store")->name("downloads.store");
    Route::get("/downloads/{downloads}", "DownloadController@show")->name("downloads.show");
    Route::get("/downloads/{downloads}/edit", "DownloadController@edit")->name("downloads.edit");
    Route::put("/downloads/{downloads}", "DownloadController@update")->name("downloads.update");
    Route::delete("/downloads/{downloads}", "DownloadController@destroy")->name("downloads.destroy");
});

Route::group(["middleware" => ["Gm"], "namespace" => "Administrator", "prefix" => "/administrator"], function () {

    Route::get("/", "HomeController@index")->name("admin.index");
    Route::get("/admin/create", "HomeController@create")->name("admin.create");
    Route::post("/admin", "HomeController@store")->name("admin.store");
    Route::get("/admin/{admin}", "HomeController@show")->name("admin.show");
    Route::get("/admin/{admin}/edit", "HomeController@edit")->name("admin.edit");
    Route::put("/admin/{admin}", "HomeController@update")->name("admin.update");
    Route::delete("/admin/{admin}", "HomeController@destroy")->name("admin.destroy");

    Route::get("/mall", "MallController@index")->name("mall.index");
    Route::get("/mall/create", "MallController@create")->name("mall.create");
    Route::post("/mall", "MallController@store")->name("mall.store");
    Route::get("/mall/{mall}", "MallController@show")->name("mall.show");
    Route::get("/mall/{mall}/edit", "MallController@edit")->name("mall.edit");
    Route::put("/mall/{mall}", "MallController@update")->name("mall.update");
    Route::delete("/mall/{mall}", "MallController@destroy")->name("mall.destroy");
    Route::post("/mall/list", "MallController@list")->name("list.index");

    Route::get("/ecommerce", "EcommerceController@index")->name("ecommerce.index");
    Route::get("/ecommerce/create", "EcommerceController@create")->name("ecommerce.create");
    Route::post("/ecommerce", "EcommerceController@store")->name("ecommerce.store");
    Route::get("/ecommerce/{ecommerce}", "EcommerceController@show")->name("ecommerce.show");
    Route::get("/ecommerce/{ecommerce}/edit", "EcommerceController@edit")->name("ecommerce.edit");
    Route::put("/ecommerce/{ecommerce}", "EcommerceController@update")->name("ecommerce.update");
    Route::delete("/ecommerce/{ecommerce}", "EcommerceController@destroy")->name("ecommerce.destroy");


    Route::get("/orders", "OrdersController@index")->name("orders.index");
    Route::get("/orders/create", "OrdersController@create")->name("orders.create");
    Route::post("/orders", "OrdersController@store")->name("orders.store");
    Route::get("/orders/{orders}", "OrdersController@show")->name("orders.show");
    Route::get("/orders/{orders}/edit", "OrdersController@edit")->name("orders.edit");
    Route::put("/orders/{orders}", "OrdersController@update")->name("orders.update");
    Route::delete("/orders/{orders}", "OrdersController@destroy")->name("orders.destroy");


    Route::get("/account", "AccountController@index")->name("account.index");
    Route::get("/account/create", "AccountController@create")->name("account.create");
    Route::post("/account", "AccountController@store")->name("account.store");
    Route::get("/account/{account}", "AccountController@show")->name("account.show");
    Route::get("/account/{account}/edit", "AccountController@edit")->name("account.edit");
    Route::put("/account/{account}", "AccountController@update")->name("account.update");
    Route::delete("/account/{account}", "AccountController@destroy")->name("account.destroy");
    Route::post("/account/list", "AccountController@list")->name("account.list");

});