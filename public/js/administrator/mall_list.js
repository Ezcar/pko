$(document).ready(function() {
    mall_list();
});

function mall_list() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#loading').fadeIn(300);
    $.ajax({
        url: "/administrator/mall/list",
        type: "POST",
        data:{},
        beforeSend: function () {
            $("#ajax_table_item_mall").fadeOut(300);
        },
        success: function (data) {

            $('#ajax_table_item_mall').html(data);
            $("#ajax_table_item_mall").fadeIn(300);
            $('#loading').fadeOut(300);
        },
        error: function (data) {
            bootbox.alert('Erro no carregamento ...internet muito lenta');
        },
        complete: function(){

        },
        statusCode: {
            404: function () {
                bootbox.alert("URL não localizada");
            }
        }
    });

}

