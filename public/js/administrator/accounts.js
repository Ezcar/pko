
function account_search(form,button) {
    var form = $(form);
    var url = form.attr('action');
    var method = form.attr('method');
    var bnt = $(button);
    var data = form.serializeArray();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#loading').fadeIn(300);
    $.ajax({
        url: url,
        type:method,
        data:data,
        beforeSend: function () {
            bnt.prop('disabled', true);
            bnt.html("Aguarde");
            $("#ajax_table_item_mall").fadeOut(300);
        },
        success: function (data) {
            bnt.prop('disabled', false);
            bnt.html("Search");
            $('#users_account').html(data);
            $("#users_account").fadeIn(300);
            $('#loading').fadeOut(300);
        },
        error: function (data) {
            bootbox.alert('Erro no carregamento ...internet muito lenta');
        },
        complete: function(){

        },
        statusCode: {
            404: function () {
                bootbox.alert("URL não localizada");
            }
        }
    });

}

