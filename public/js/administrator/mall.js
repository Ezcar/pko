

function create() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "/administrator/mall/create",
        type: "GET",
        data: {},
        beforeSend: function() {
            $('#loading').fadeIn(300);
        },
        success: function(data) {
            $('#modal-body').html(data);
            $('#modal-title').html('Create new item mall');
            $('#loading').fadeOut(300);
        },
        error: function(data) {
            alert("Error" + data);
        },
        statusCode: {
            404: function() {
                alert("URL não localizada");
            }
        },
        complete: function() {
            $('#modal').modal('show');
        }
    });

}


function store(form, button) {

    var form = $(form);
    var url = form.attr('action');
    var method = form.attr('method');
    var bnt = $(button);
    var data = form.serializeArray();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#loading').show();
    $.ajax({
        url: url,
        type: method,
        data: data,
        beforeSend: function () {
            bnt.prop('disabled', true);
            bnt.html("Aguarde");
            $('#alert').html('');
            $('#alert').hide();

        },
        success: function (data) {
            $('#loading').hide();
            bnt.prop('disabled', false);
            bnt.html("Confirm");
            console.log(data);



            if (data.success) {
                bootbox.alert(data.success);
            }

            $.each(data.errors, function (key, value) {
                $('.alert-danger').show();
                $('.alert-danger').append('<p>' + value + '</p>');
            });
        },
        error: function (data) {

        },
        statusCode: {
            404: function () {
                bootbox.alert("Error url");
            }
        },
        complete: function () {
            mall_list();
        }
    });
}

function edit(id,button) {
    var button = $(button);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: "/administrator/mall/"+id+"/edit/",
        type: "GET",
        beforeSend: function() {
            button.prop('disabled', true);
            $('#loading').fadeIn(300);
            button.html("<i class='fa fa-circle-o-notch fa-spin'></i>  Waiting");
        },
        success: function(data) {
            button.prop('disabled', false);
            $('#modal-body').html(data);
            $('#modal-title').html('Edit item mall');
            $('#loading').fadeOut(300);
            button.html("Edit");

            if (data.success) {
                bootbox.alert(data.success);
            }
        },
        error: function(data) {
            alert("Error" + data);
        },
        statusCode: {
            404: function() {
                alert("URL não localizada");
            }
        },
        complete: function() {
            $('#modal').modal('show');
        }
    });

}


function delete_mall(id,bnt) {

    var bnt = $(bnt);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    bootbox.confirm({
        message: "You are sure about it?",
        buttons: {
            confirm: {
                label: 'Yes ',
                className: 'btn btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn btn-danger'
            }
        },
        callback: function(result) {

            if (result == true) {
                $('#loading').fadeIn(300);
                $.ajax({
                    url: "/administrator/mall/" + id,
                    type: "DELETE",
                    data: {
                        id: id,
                        _method: "DELETE"
                    },
                    beforeSend: function() {
                        bnt.prop('disabled', true);
                    },
                    success: function(data) {
                        bnt.prop('disabled', false);
                        if (data.success) {
                            bootbox.alert(data.success);
                        }

                        if (data.errors) {
                            bootbox.alert(data.errors);
                        }
                        $('#loading').fadeOut(300);
                    },
                    error: function(data) {
                        alert(data);
                    },
                    statusCode: {
                        404: function() {
                            alert("URL não localizada");
                        }
                    },
                    complete: function() {
                         mall_list();
                        $('#loading').fadeOut(300);
                        $('#modal').modal('hide');
                    }
                });
            }
        }
    });
}