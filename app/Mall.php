<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
/**
 * Class Mall
 * @package App
 */
class Mall extends Model
{
    /**
     * @var string
     */
    public $table = 'itemMall';
    /**
     * @var string
     */
    protected $connection = 'DATABASE_AUCTION';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var string
     */
    public $primaryKey = 'MallID';
    /**
     * @var array
     */
    protected $fillable = ['ItemName', 'ItemPrice', 'ItemDesc', 'Quota', 'Icon', 'cType', 'ItemID', 'quantity', 'category'];
    /**
     * @param int $mall_points
     * @param int $ItemPrice
     * @return int
     */
    static public function subtract_value(int $mall_points, int $ItemPrice)
    {
        return ($mall_points - $ItemPrice);
    }
    /**
     * @param $actual
     * @param $price
     * @return bool
     */
    static public function checkMallPoints(int $actual,int $price)
    {
        if (isset($actual) && !empty($actual) && isset($price) && !empty($price)) {
            if ($actual >= $price) {
                return true;
            }
        }
        return false;
    }

}
