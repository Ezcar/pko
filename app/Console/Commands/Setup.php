<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pko:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = \App\AccountServer::all();
        $this->output->progressStart(count($accounts));
        foreach ($accounts as $account) {


            if (isset($account->name) && !empty($account->name && !is_null($account->name))) {
                $gm = \App\AccountGamer::find($account->id);
                $level = isset($gm->gm)?$gm->gm:0;
                $acc = array(
                    'name' => $account->name,
                    'email' => $account->email,
                    'password' => \Hash::make($account->originalPassword),
                    'act_id' => $account->id,
                    'gm' => $level);
                $user = \App\User::updateOrCreate(['name' => $account->name], $acc);
                if ($user) {
                    $this->output->progressAdvance();
                }
            }
        }
        $this->output->progressFinish();
    }
}
