<?php
namespace App\Http\Controllers\Administrator;
use App\Http\Controllers\Controller;
use App\Mall;
use Illuminate\Http\Request;
class MallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('administrator.mall.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( )
    {

        return view('administrator.mall.create-update', get_defined_vars());
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->has('MallID') && !empty($request->get('MallID'))){

               return $this->update($request,$request->get('MallID'));



        }
        $validator = \Validator::make($request->all(), [
            'ItemPrice' => 'required|numeric',
            'ItemDesc' => 'required|string',
            'Quota' => 'required|integer',
            'cType' => 'required|string',
            'ItemID' => 'required|integer',
            'quantity' => 'required|integer',
            'category' => 'required|string',
            'ItemName' => 'required|string',
        ]);
        if ($validator->fails()) {
            return \response()->json(['errors' => $validator->errors()->all()]);
        }
        $itemMall = new \App\Mall();
        $itemMall->ItemName = trim($request->get('ItemName'));
        $itemMall->ItemPrice = trim($request->get('ItemPrice'));
        $itemMall->ItemDesc = trim($request->get('ItemDesc'));
        $itemMall->Quota = trim($request->get('Quota'));
        $itemMall->Icon = trim($this->GetMallIcon($request->get('ItemID')));
        $itemMall->cType = trim($request->get('cType'));
        $itemMall->ItemID = trim($request->get('ItemID'));
        $itemMall->quantity = trim($request->get('quantity'));
        $itemMall->category = trim($request->get('category'));
        if ($itemMall->save()) {
            return \response()->json(['success' => 'Record is successfully added']);
        } else {
            return \response()->json(['errors' => 'Record is not successfully added']);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = \App\Mall::find($id);
        return view('administrator.mall.create-update', get_defined_vars());
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $itemMall = \App\Mall::find($id);
        $itemMall->ItemName = trim($request->get('ItemName'));
        $itemMall->ItemPrice = trim($request->get('ItemPrice'));
        $itemMall->ItemDesc = trim($request->get('ItemDesc'));
        $itemMall->Quota = trim($request->get('Quota'));
        $itemMall->Icon = trim($this->GetMallIcon($request->get('ItemID')));
        $itemMall->cType = trim($request->get('cType'));
        $itemMall->ItemID = trim($request->get('ItemID'));
        $itemMall->quantity = trim($request->get('quantity'));
        $itemMall->category = trim($request->get('category'));
        if ($itemMall->save()) {
            return \response()->json(['success' => 'Record is updated']);
        } else {
            return \response()->json(['errors' => 'Record is not updated']);
        }

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemMall = \App\Mall::find($id);

        if ($itemMall->delete()) {
            return \response()->json(['success' => 'Record is deleted']);
        } else {
            return \response()->json(['errors' => 'Record is not deleted']);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $malls = \App\Mall::all();
        return view('administrator.mall.list', get_defined_vars());
    }

    /**
     * @param $Item
     * @return string
     */
    public function GetMallIcon($Item)
    {
        $ItemFile = 'data/iteminfo.txt';
        $FileDump = file($ItemFile);
        $Icon = '';
        foreach ($FileDump as $value) {
            $Explode = explode("\t", $value);
            if ($Explode[0] == $Item) {
                $Icon .= $Explode[1];
            }
        }
        return $Icon;
    }
}
