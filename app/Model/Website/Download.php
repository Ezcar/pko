<?php

namespace App\Model\Website;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    protected $table = 'downloads';
    protected $guarded=['id'];
}
