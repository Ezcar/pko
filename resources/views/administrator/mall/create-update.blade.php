<div class="card">
    <div class="card-header"></div>

    <div class="card-body">
        <div id="alert" class="alert alert-danger" style="display:none"></div>
        <form id="form" method="post" action="{{route('mall.store')}}">
           <input type="hidden" name="MallID" value="{{isset($item->MallID)?$item->MallID:''}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="ItemName">Item name </label>
                        <input value="{{isset($item->ItemName)?$item->ItemName:null}}"  name="ItemName" type="text" class="form-control" id="ItemName" placeholder="Item name">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="ItemID">ItemInfo ID</label>
                        <input value="{{isset($item->ItemID)?$item->ItemID:null}}" name="ItemID" min="1" type="number" class="form-control" id="ItemID" placeholder="ItemInfo ID">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="quantity"> Quantity</label>
                        <input value="{{isset($item->quantity)?$item->quantity:null}}" name="quantity" type="number" min="1" max="999" pattern="[0-9]" class="form-control"
                               id="Quantity" placeholder="Item Quantity">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="ItemPrice">Price</label>
                        <input value="{{isset($item->ItemPrice)?$item->ItemPrice:null}}" name="ItemPrice" type="number" min="1" class="form-control" id="ItemPrice" placeholder="Item Price">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group">
                        <label for="ItemDesc">Item Description</label>
                        <textarea  name="ItemDesc" class="form-control" id="ItemDesc"
                                  rows="3">{{isset($item->ItemDesc)?$item->ItemDesc:null}}</textarea>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="Quota">Item Quota</label>
                        <input value="{{isset($item->Quota)?$item->Quota:null}}" type="number" min="1" class="form-control" id="Quota" name="Quota"
                               placeholder="Item quota">
                        <small id="Quota" class="form-text text-muted"> -1 = unlimited.</small>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="cType">Item Type</label>
                        <select name="cType" class="form-control" id="cType">
                            <option selected value="ItemMall">Item Mall</option>
                            <option value="AwardItem">Award Item</option>
                        </select>
                    </div>

                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select name="category" class="form-control" id="category">
                            <option selected value="promotion">Promotion</option>
                            <option value="pet">Pet</option>
                            <option value="forging">Forging</option>
                            <option value="equipment">Equipment</option>
                            <option value="decoration">Decoration</option>
                            <option value="miscelaneous">Miscelaneous</option>
                        </select>
                    </div>

                </div>
                <div class="col-md-3">


                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <button onclick="store(this.form,this)" type="button" class="btn btn-primary float-right">
                        Confirm
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>