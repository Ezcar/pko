@auth
    @if(count($malls)>0)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body">


                        <div class="table-responsive-sm">
                            <table id="item_mall" class="table">
                                <thead>
                                <tr>
                                    <th>Icon</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Quota</th>
                                    <th>quantity</th>
                                    <th>category</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($malls as $mall)

                                    <tr>
                                        <td><img src="{{env('APP_URL') . '/img/icons/'.trim($mall->Icon).'.png'}}"></td>
                                        <td> {{ $mall->ItemName }}</td>
                                        <td> {{ $mall->ItemDesc }}</td>
                                        <td> {{ $mall->ItemPrice }}</td>
                                        <td> {{ $mall->Quota }} </td>
                                        <td> {{ $mall->quantity }}</td>
                                        <td> {{ $mall->category }}</td>
                                        <td>
                                            <button onclick="edit('{{$mall->MallID}}',this)" type="button" class="btn btn-outline-primary">Edit
                                            </button>
                                            <button onclick="delete_mall('{{$mall->MallID}}',this)" type="button" class="btn btn-outline-primary">delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    @endif
    <script>
        $(document).ready(function () {
            $('#item_mall').DataTable();
        });
    </script>
@endauth