@extends('layouts.dashboard')

@section('js')
    <script src="/js/administrator/mall.js"></script>
    <script src="/js/administrator/mall_list.js"></script>
    <!-- DataTables JavaScript -->
    <script src="/dashboard/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/dashboard/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/dashboard/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
@endsection
@section('content')

    <div style="display: none" id="loading" class="pull-right"><img style="width: 100px; height: 100px" class="float-right" src="/img/ajax/loading.gif"></div>
    <h1 class="page-header">E-commerce Mall </h1>

    <button onclick="create(this)" type="button" class="btn btn-primary">New</button>
    <hr>
    <div id="ajax_table_item_mall"></div>

@endsection