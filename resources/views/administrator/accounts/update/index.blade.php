@extends('layouts.dashboard')

@section('js')
    <script src="/js/administrator/accounts.js"></script>
    <!-- DataTables JavaScript -->
    <script src="/dashboard/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/dashboard/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/dashboard/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
@endsection
@section('content')

    <div style="display: none" id="loading" class="pull-right"><img style="width: 100px; height: 100px" class="float-right" src="/img/ajax/loading.gif"></div>
    <h1 class="page-header">Search for users accounts </h1>

    <form id="form"  method="post" action="{{route('account.list')}}">
    <input style="margin-bottom: 20px" class="form-control" type="text" placeholder="name  login or E-mail search" name="name_email">
        <button onclick="account_search(this.form,this)" type="button" class="btn btn-primary">Search</button>
    </form>

    <hr>

    <div id="users_account"></div>

@endsection