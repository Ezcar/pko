@if(isset($accounts_search) && count($accounts_search)>0)


    <table class="table table-striped" id="accounts_table" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>name</th>
            <th>Mall</th>
            <th>Credits</th>
            <th>last_ip</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($accounts_search as $accounts)
            <tr>
                <td>{{isset($accounts->act_id)?$accounts->act_id:null}}</td>
                <td>{{isset($accounts->name)?$accounts->name:null}}</td>
                <td>{{isset($accounts->account->mall_points)?$accounts->account->mall_points:0}}</td>
                <td>{{isset($accounts->account->credits)?$accounts->account->credits:0}}</td>
                <td>{{isset($accounts->account->last_ip)?$accounts->account->last_ip:0}}</td>
                <td>
                    <button onclick="" type="button" class="btn btn-outline-primary">Edit
                    </button>
                    <button onclick="" type="button" class="btn btn-outline-primary">Ban
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <script>
        $(document).ready(function () {
            $('#accounts_table').DataTable();
        });
    </script>
@else

    <div class="alert alert-danger">
        <strong>Danger!</strong> Nothing here ... like your life
    </div>
@endif