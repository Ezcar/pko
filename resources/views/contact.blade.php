@extends('layouts.app')
@section('content')
	<div style="margin-bottom: 20px !important;" class="card">
		<div class="card-header">Contact us </div>
		<div class="card-body">
				<form action="/action_page.php">
					<div class="form-group">
						<label for="title">Title</label>
						<input type="title" class="form-control" id="title" required>
					</div>


					<div class="form-group">
						<label for="type">Type</label>
						<select class="form-control" id="type" required>
							<option name="bug">Bug report</option>
							<option name="hack">Hack(er) report </option>
							<option name="scam">Scam(mer) report </option>
							<option name="sugg"> Suggestion </option>
							<option name="other"> Other </option>
						</select>
					</div>
					<div class="form-group">
						<label for="comment">Comment:</label>
						<textarea class="form-control" rows="5" id="comment"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>


		</div>
	</div>
@endsection